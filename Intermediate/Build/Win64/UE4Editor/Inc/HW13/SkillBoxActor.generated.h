// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HW13_SkillBoxActor_generated_h
#error "SkillBoxActor.generated.h already included, missing '#pragma once' in SkillBoxActor.h"
#endif
#define HW13_SkillBoxActor_generated_h

#define HW13_Source_HW13_SkillBoxActor_h_12_SPARSE_DATA
#define HW13_Source_HW13_SkillBoxActor_h_12_RPC_WRAPPERS
#define HW13_Source_HW13_SkillBoxActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define HW13_Source_HW13_SkillBoxActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASkillBoxActor(); \
	friend struct Z_Construct_UClass_ASkillBoxActor_Statics; \
public: \
	DECLARE_CLASS(ASkillBoxActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW13"), NO_API) \
	DECLARE_SERIALIZER(ASkillBoxActor)


#define HW13_Source_HW13_SkillBoxActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASkillBoxActor(); \
	friend struct Z_Construct_UClass_ASkillBoxActor_Statics; \
public: \
	DECLARE_CLASS(ASkillBoxActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW13"), NO_API) \
	DECLARE_SERIALIZER(ASkillBoxActor)


#define HW13_Source_HW13_SkillBoxActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASkillBoxActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASkillBoxActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASkillBoxActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASkillBoxActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASkillBoxActor(ASkillBoxActor&&); \
	NO_API ASkillBoxActor(const ASkillBoxActor&); \
public:


#define HW13_Source_HW13_SkillBoxActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASkillBoxActor(ASkillBoxActor&&); \
	NO_API ASkillBoxActor(const ASkillBoxActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASkillBoxActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASkillBoxActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASkillBoxActor)


#define HW13_Source_HW13_SkillBoxActor_h_12_PRIVATE_PROPERTY_OFFSET
#define HW13_Source_HW13_SkillBoxActor_h_9_PROLOG
#define HW13_Source_HW13_SkillBoxActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW13_Source_HW13_SkillBoxActor_h_12_PRIVATE_PROPERTY_OFFSET \
	HW13_Source_HW13_SkillBoxActor_h_12_SPARSE_DATA \
	HW13_Source_HW13_SkillBoxActor_h_12_RPC_WRAPPERS \
	HW13_Source_HW13_SkillBoxActor_h_12_INCLASS \
	HW13_Source_HW13_SkillBoxActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HW13_Source_HW13_SkillBoxActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW13_Source_HW13_SkillBoxActor_h_12_PRIVATE_PROPERTY_OFFSET \
	HW13_Source_HW13_SkillBoxActor_h_12_SPARSE_DATA \
	HW13_Source_HW13_SkillBoxActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	HW13_Source_HW13_SkillBoxActor_h_12_INCLASS_NO_PURE_DECLS \
	HW13_Source_HW13_SkillBoxActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HW13_API UClass* StaticClass<class ASkillBoxActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HW13_Source_HW13_SkillBoxActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
