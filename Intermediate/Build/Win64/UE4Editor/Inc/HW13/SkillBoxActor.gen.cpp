// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HW13/SkillBoxActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSkillBoxActor() {}
// Cross Module References
	HW13_API UClass* Z_Construct_UClass_ASkillBoxActor_NoRegister();
	HW13_API UClass* Z_Construct_UClass_ASkillBoxActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_HW13();
// End Cross Module References
	void ASkillBoxActor::StaticRegisterNativesASkillBoxActor()
	{
	}
	UClass* Z_Construct_UClass_ASkillBoxActor_NoRegister()
	{
		return ASkillBoxActor::StaticClass();
	}
	struct Z_Construct_UClass_ASkillBoxActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASkillBoxActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_HW13,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASkillBoxActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SkillBoxActor.h" },
		{ "ModuleRelativePath", "SkillBoxActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASkillBoxActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASkillBoxActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASkillBoxActor_Statics::ClassParams = {
		&ASkillBoxActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASkillBoxActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASkillBoxActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASkillBoxActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASkillBoxActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASkillBoxActor, 2075064955);
	template<> HW13_API UClass* StaticClass<ASkillBoxActor>()
	{
		return ASkillBoxActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASkillBoxActor(Z_Construct_UClass_ASkillBoxActor, &ASkillBoxActor::StaticClass, TEXT("/Script/HW13"), TEXT("ASkillBoxActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASkillBoxActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
