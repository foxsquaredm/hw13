// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Commandlets/Commandlet.h"
#include "ModifyAssetsCommandlet.generated.h"

UCLASS()
class SKILLBOXPLUGIN_API UModifyAssetsCommandlet : public UCommandlet
{
	GENERATED_BODY()

	virtual int32 Main(const FString& Params) override;

	void ProcessAssets(TArray<FString> RootDirectories);

	void ModifyLod(UObject* AssetInstance);

	void SaveAsset(UObject* AssetInstance);
};
