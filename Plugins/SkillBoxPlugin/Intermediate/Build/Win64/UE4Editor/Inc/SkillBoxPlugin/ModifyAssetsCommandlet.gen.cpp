// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SkillBoxPlugin/Public/ModifyAssetsCommandlet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeModifyAssetsCommandlet() {}
// Cross Module References
	SKILLBOXPLUGIN_API UClass* Z_Construct_UClass_UModifyAssetsCommandlet_NoRegister();
	SKILLBOXPLUGIN_API UClass* Z_Construct_UClass_UModifyAssetsCommandlet();
	ENGINE_API UClass* Z_Construct_UClass_UCommandlet();
	UPackage* Z_Construct_UPackage__Script_SkillBoxPlugin();
// End Cross Module References
	void UModifyAssetsCommandlet::StaticRegisterNativesUModifyAssetsCommandlet()
	{
	}
	UClass* Z_Construct_UClass_UModifyAssetsCommandlet_NoRegister()
	{
		return UModifyAssetsCommandlet::StaticClass();
	}
	struct Z_Construct_UClass_UModifyAssetsCommandlet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UModifyAssetsCommandlet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommandlet,
		(UObject* (*)())Z_Construct_UPackage__Script_SkillBoxPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UModifyAssetsCommandlet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ModifyAssetsCommandlet.h" },
		{ "ModuleRelativePath", "Public/ModifyAssetsCommandlet.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UModifyAssetsCommandlet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UModifyAssetsCommandlet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UModifyAssetsCommandlet_Statics::ClassParams = {
		&UModifyAssetsCommandlet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UModifyAssetsCommandlet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UModifyAssetsCommandlet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UModifyAssetsCommandlet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UModifyAssetsCommandlet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UModifyAssetsCommandlet, 196742486);
	template<> SKILLBOXPLUGIN_API UClass* StaticClass<UModifyAssetsCommandlet>()
	{
		return UModifyAssetsCommandlet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UModifyAssetsCommandlet(Z_Construct_UClass_UModifyAssetsCommandlet, &UModifyAssetsCommandlet::StaticClass, TEXT("/Script/SkillBoxPlugin"), TEXT("UModifyAssetsCommandlet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UModifyAssetsCommandlet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
