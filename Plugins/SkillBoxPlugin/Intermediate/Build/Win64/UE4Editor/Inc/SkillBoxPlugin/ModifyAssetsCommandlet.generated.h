// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKILLBOXPLUGIN_ModifyAssetsCommandlet_generated_h
#error "ModifyAssetsCommandlet.generated.h already included, missing '#pragma once' in ModifyAssetsCommandlet.h"
#endif
#define SKILLBOXPLUGIN_ModifyAssetsCommandlet_generated_h

#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_SPARSE_DATA
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_RPC_WRAPPERS
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUModifyAssetsCommandlet(); \
	friend struct Z_Construct_UClass_UModifyAssetsCommandlet_Statics; \
public: \
	DECLARE_CLASS(UModifyAssetsCommandlet, UCommandlet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SkillBoxPlugin"), NO_API) \
	DECLARE_SERIALIZER(UModifyAssetsCommandlet)


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUModifyAssetsCommandlet(); \
	friend struct Z_Construct_UClass_UModifyAssetsCommandlet_Statics; \
public: \
	DECLARE_CLASS(UModifyAssetsCommandlet, UCommandlet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SkillBoxPlugin"), NO_API) \
	DECLARE_SERIALIZER(UModifyAssetsCommandlet)


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UModifyAssetsCommandlet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UModifyAssetsCommandlet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UModifyAssetsCommandlet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UModifyAssetsCommandlet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UModifyAssetsCommandlet(UModifyAssetsCommandlet&&); \
	NO_API UModifyAssetsCommandlet(const UModifyAssetsCommandlet&); \
public:


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UModifyAssetsCommandlet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UModifyAssetsCommandlet(UModifyAssetsCommandlet&&); \
	NO_API UModifyAssetsCommandlet(const UModifyAssetsCommandlet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UModifyAssetsCommandlet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UModifyAssetsCommandlet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UModifyAssetsCommandlet)


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_PRIVATE_PROPERTY_OFFSET
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_9_PROLOG
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_PRIVATE_PROPERTY_OFFSET \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_SPARSE_DATA \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_RPC_WRAPPERS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_INCLASS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_PRIVATE_PROPERTY_OFFSET \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_SPARSE_DATA \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_INCLASS_NO_PURE_DECLS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKILLBOXPLUGIN_API UClass* StaticClass<class UModifyAssetsCommandlet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_ModifyAssetsCommandlet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
