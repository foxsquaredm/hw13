// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKILLBOXPLUGIN_MyNewActor_generated_h
#error "MyNewActor.generated.h already included, missing '#pragma once' in MyNewActor.h"
#endif
#define SKILLBOXPLUGIN_MyNewActor_generated_h

#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_SPARSE_DATA
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_RPC_WRAPPERS
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyNewActor(); \
	friend struct Z_Construct_UClass_AMyNewActor_Statics; \
public: \
	DECLARE_CLASS(AMyNewActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkillBoxPlugin"), NO_API) \
	DECLARE_SERIALIZER(AMyNewActor)


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyNewActor(); \
	friend struct Z_Construct_UClass_AMyNewActor_Statics; \
public: \
	DECLARE_CLASS(AMyNewActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkillBoxPlugin"), NO_API) \
	DECLARE_SERIALIZER(AMyNewActor)


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyNewActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyNewActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyNewActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyNewActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyNewActor(AMyNewActor&&); \
	NO_API AMyNewActor(const AMyNewActor&); \
public:


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyNewActor(AMyNewActor&&); \
	NO_API AMyNewActor(const AMyNewActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyNewActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyNewActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyNewActor)


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_PRIVATE_PROPERTY_OFFSET
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_9_PROLOG
#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_PRIVATE_PROPERTY_OFFSET \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_SPARSE_DATA \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_RPC_WRAPPERS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_INCLASS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_PRIVATE_PROPERTY_OFFSET \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_SPARSE_DATA \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_INCLASS_NO_PURE_DECLS \
	HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKILLBOXPLUGIN_API UClass* StaticClass<class AMyNewActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HW13_Plugins_SkillBoxPlugin_Source_SkillBoxPlugin_Public_MyNewActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
